package ec.com.enterprise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan("ec.com.enterprise.model")
@SpringBootApplication
public class EnterpriseRootApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnterpriseRootApplication.class, args);
	}

}
