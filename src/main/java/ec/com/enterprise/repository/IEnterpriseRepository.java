/**
 * 
 */
package ec.com.enterprise.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ec.com.enterprise.model.Enterprise;

/**
 * Enterprise repository.
 * 
 * @author mpilacuan
 *
 */
public interface IEnterpriseRepository extends JpaRepository<Enterprise, Long> {

}
