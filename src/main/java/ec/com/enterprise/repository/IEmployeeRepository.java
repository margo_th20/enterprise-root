/**
 * 
 */
package ec.com.enterprise.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ec.com.enterprise.model.Employee;

/**
 * Employee repository.
 * 
 * @author mpilacuan
 *
 */
public interface IEmployeeRepository extends JpaRepository<Employee, Long>{

}
