/**
 * 
 */
package ec.com.enterprise.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ec.com.enterprise.model.Department;

/**
 * Department repository.
 * 
 * @author mpilacuan
 *
 */
public interface IDepartmentRepository extends JpaRepository<Department, Long> {
	
	@Query("SELECT d FROM Department d WHERE d.idEnterprise = ?1 ")
	Collection<Department> findDepartmentByIdEnterprise(Long idEnterprise);

}
