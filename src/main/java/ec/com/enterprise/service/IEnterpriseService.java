/**
 * 
 */
package ec.com.enterprise.service;

import java.util.Collection;

import ec.com.enterprise.model.Enterprise;

/**
 * Enterprise service.
 * 
 * @author mpilacuan
 *
 */
public interface IEnterpriseService {

	/**
	 * Find all enterprises.
	 * 
	 * @return collection
	 */
	Collection<Enterprise> findAllEnterprises();

	/**
	 * Save or update enterprise.
	 * 
	 * @param enterprise
	 * @return enterprise
	 */
	Enterprise createOrUpadateEnterprise(Enterprise enterprise);

}
