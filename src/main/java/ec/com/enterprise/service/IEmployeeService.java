/**
 * 
 */
package ec.com.enterprise.service;

import java.util.Collection;

import ec.com.enterprise.model.Employee;

/**
 * Employee service.
 * 
 * @author mpilacuan
 *
 */
public interface IEmployeeService {

	/**
	 * Find all employees.
	 * 
	 * @return collection
	 */
	Collection<Employee> findAllEmployees();

	/**
	 * Save or update employee.
	 * 
	 * @param employee
	 * @return employee
	 */
	Employee createOrUpadateEmployee(Employee employee);
}
