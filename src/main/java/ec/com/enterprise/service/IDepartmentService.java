package ec.com.enterprise.service;

import java.util.Collection;

import ec.com.enterprise.model.Department;

/**
 * Department service.
 * 
 * @author mpilacuan.
 *
 */
public interface IDepartmentService {

	/**
	 * Find all departments.
	 * 
	 * @param idEnterprise
	 * @return collection
	 */
	Collection<Department> findAllDepartments(Long idEnterprise);

	/**
	 * Save or update department.
	 * 
	 * @param department
	 * @return department
	 */
	Department createOrUpadateDepartment(Department department);
}
