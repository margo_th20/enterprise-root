/**
 * 
 */
package ec.com.enterprise.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.com.enterprise.model.Employee;
import ec.com.enterprise.repository.IEmployeeRepository;
import ec.com.enterprise.service.IEmployeeService;

/**
 * Employee service implementation.
 * 
 * @author mpilacuan
 *
 */
@Service
public class EmployeeServiceImpl implements IEmployeeService {

	@Autowired
	private IEmployeeRepository employeeRepository;

	@Override
	public Collection<Employee> findAllEmployees() {
		return employeeRepository.findAll();
	}

	@Override
	public Employee createOrUpadateEmployee(Employee employee) {
		return employeeRepository.save(employee);
	}

}
