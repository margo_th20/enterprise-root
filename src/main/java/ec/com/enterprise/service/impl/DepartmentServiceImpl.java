/**
 * 
 */
package ec.com.enterprise.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.com.enterprise.model.Department;
import ec.com.enterprise.repository.IDepartmentRepository;
import ec.com.enterprise.service.IDepartmentService;

/**
 * Department service implementation.
 * 
 * @author mpilacuan
 *
 */
@Service
public class DepartmentServiceImpl implements IDepartmentService {

	@Autowired
	private IDepartmentRepository departmentRepository;

	@Override
	public Collection<Department> findAllDepartments(Long idEnterprise) {
		return departmentRepository.findDepartmentByIdEnterprise(idEnterprise);
	}

	@Override
	public Department createOrUpadateDepartment(Department department) {
		return departmentRepository.save(department);
	}

}
