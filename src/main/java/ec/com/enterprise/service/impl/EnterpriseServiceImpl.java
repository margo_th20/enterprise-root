/**
 * 
 */
package ec.com.enterprise.service.impl;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.com.enterprise.model.Enterprise;
import ec.com.enterprise.repository.IEnterpriseRepository;
import ec.com.enterprise.service.IEnterpriseService;

/**
 * Enterprise service implementation.
 * 
 * @author mpilacuan
 *
 */
@Service
public class EnterpriseServiceImpl implements IEnterpriseService {

	@Autowired
	private IEnterpriseRepository enterpriseRepository;

	@Override
	public Collection<Enterprise> findAllEnterprises() {
		return enterpriseRepository.findAll();
	}

	@Override
	public Enterprise createOrUpadateEnterprise(Enterprise enterprise) {
		return enterpriseRepository.save(enterprise);
	}

}
