/**
 * 
 */
package ec.com.enterprise.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Department Vo.
 * 
 * @author mpilacuan
 *
 */
@Data
@AllArgsConstructor
public class DepartmentVo {

	private Long id;

	private String description;

	private String name;

	private String phone;

	private Long idEnterprise;

}
