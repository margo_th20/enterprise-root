/**
 * 
 */
package ec.com.enterprise.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Enterprise Vo.
 * 
 * @author mpilacuan
 *
 */
@Data
@AllArgsConstructor
public class EnterpriseVo {

	private Long id;

	private String address;

	private String name;

	private String phone;
}
