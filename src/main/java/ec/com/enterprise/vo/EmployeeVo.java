package ec.com.enterprise.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Employee Vo.
 * 
 * @author mpilacuan
 *
 */
@Data
@AllArgsConstructor
public class EmployeeVo {

	private Long id;

	private Integer age;

	private String email;

	private String name;

	private String position;

	private String surname;

}
