/**
 * 
 */
package ec.com.enterprise.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.com.enterprise.model.Department;
import ec.com.enterprise.service.IDepartmentService;
import ec.com.enterprise.vo.DepartmentVo;

/**
 * Departments controller.
 * 
 * @author mpilacuan
 *
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/department/")
public class DepartmentController {

	@Autowired
	private IDepartmentService departmentService;

	@GetMapping(value = "/findAllDepartments/{idEnterprise}")
	public ResponseEntity<Collection<Department>> findAllDepartments(@PathVariable("idEnterprise") Long idEnterprise ) {
		return ResponseEntity.ok(departmentService.findAllDepartments(idEnterprise));
	}

	@PostMapping(value = "/createOrUpdateDepartment")
	public ResponseEntity<DepartmentVo> createOrUpdateDepartment(@RequestBody DepartmentVo departmentVo) {
		Department request = new Department();
		request.setId(departmentVo.getId());
		request.setDescription(departmentVo.getDescription());
		request.setName(departmentVo.getName());
		request.setPhone(departmentVo.getPhone());
		request.setIdEnterprise(departmentVo.getIdEnterprise());
		departmentService.createOrUpadateDepartment(request);
		DepartmentVo resultVo = new DepartmentVo(request.getId(), request.getDescription(), request.getName(),
				request.getPhone(), request.getIdEnterprise());
		return ResponseEntity.ok(resultVo);
	}

}
