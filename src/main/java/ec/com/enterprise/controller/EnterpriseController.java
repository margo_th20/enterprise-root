/**
 * 
 */
package ec.com.enterprise.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.com.enterprise.model.Enterprise;
import ec.com.enterprise.service.IEnterpriseService;
import ec.com.enterprise.vo.EnterpriseVo;

/**
 * Enterprise controller.
 * 
 * @author mpilacuan
 *
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/enterprise/")
public class EnterpriseController {

	@Autowired
	private IEnterpriseService enterpriseService;

	@GetMapping(value = "/findAllEnterprise")
	public ResponseEntity<Collection<Enterprise>> findAllEnterprise() {
		return ResponseEntity.ok(enterpriseService.findAllEnterprises());
	}

	@PostMapping(value = "/createOrUpdateEnterprise")
	public ResponseEntity<EnterpriseVo> createOrUpdateEnterprise(@RequestBody EnterpriseVo enterprise) {
		Enterprise request = new Enterprise();
		request.setAddress(enterprise.getAddress());
		request.setId(enterprise.getId());
		request.setName(enterprise.getName());
		request.setPhone(enterprise.getPhone());
		enterpriseService.createOrUpadateEnterprise(request);
		EnterpriseVo resultVo = new EnterpriseVo(request.getId(), request.getAddress(), request.getName(),
				request.getPhone());
		return ResponseEntity.ok(resultVo);
	}

}
