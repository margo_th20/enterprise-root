/**
 * 
 */
package ec.com.enterprise.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.com.enterprise.model.Employee;
import ec.com.enterprise.service.IEmployeeService;
import ec.com.enterprise.vo.EmployeeVo;

/**
 * Employee controller.
 * 
 * @author mpilacuan
 *
 */
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/employee/")
public class EmployeeController {

	@Autowired
	private IEmployeeService employeeService;

	@GetMapping(value = "/findAllEmployees")
	public ResponseEntity<Collection<Employee>> findAllEmployee() {
		return ResponseEntity.ok(employeeService.findAllEmployees());
	}

	@PostMapping(value = "/createOrUpdateEmployee")
	public ResponseEntity<EmployeeVo> createOrUpdateEmployee(@RequestBody EmployeeVo employeeVo) {
		Employee request = new Employee();
		request.setId(employeeVo.getId());
		request.setAge(employeeVo.getAge());
		request.setEmail(employeeVo.getEmail());
		request.setName(employeeVo.getName());
		request.setSurname(employeeVo.getSurname());
		request.setPosition(employeeVo.getPosition());
		employeeService.createOrUpadateEmployee(request);
		EmployeeVo resultVo = new EmployeeVo(request.getId(), request.getAge(), request.getEmail(), request.getName(),
				request.getPosition(), request.getSurname());
		return ResponseEntity.ok(resultVo);
	}

}
