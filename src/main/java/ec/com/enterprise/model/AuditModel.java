/**
 * 
 */
package ec.com.enterprise.model;

import java.util.Date;

import javax.persistence.Column;

import lombok.Data;

/**
 * Audit model.
 * 
 * @author mpilacuan
 *
 */
@Data
public class AuditModel {

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Column(name = "modified_date")
	private Date modifiedDate;

	private Boolean status;
}
