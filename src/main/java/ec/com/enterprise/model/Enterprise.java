/**
 * 
 */
package ec.com.enterprise.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Enterprises model.
 * 
 * @author mpilacuan
 *
 */
@Entity
@Table(name = "enterprises")
@Data
@EqualsAndHashCode(callSuper = false)
public class Enterprise extends AuditModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String address;
	
	private String name;
	
	private String phone;
	
	
}
