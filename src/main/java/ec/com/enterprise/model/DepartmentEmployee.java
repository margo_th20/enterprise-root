/**
 * 
 */
package ec.com.enterprise.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Departments Employee model.
 * 
 * @author mpilacuan
 *
 */
@Entity
@Table(name = "departments_employees")
@Data
@EqualsAndHashCode(callSuper = false)
public class DepartmentEmployee extends AuditModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "id_department")
	private Long idDepartment;

	@Column(name = "id_employee")
	private Long idEmployee;

}
