/**
 * 
 */
package ec.com.enterprise.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Employees model.
 * 
 * @author mpilacuan
 *
 */
@Entity
@Table(name = "employees")
@Data
@EqualsAndHashCode(callSuper = false)
public class Employee extends AuditModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private Integer age;

	private String email;

	private String name;

	private String position;

	private String surname;
}
